# README #

Este README auxilia o usuário a utilizar o projeto

### Para que serve este repositório? ###

* Este repositório tem como finalidade a simulação da "condição de corrida" em C e como tratá-la.
* Versão 1.0

### Preparando ambiente e executando ###

* Clonar projeto ou baixar .zip;
* Instalar o compilador GCC utilizando o comando apt-get install gcc;
* Compilar o arquivo so_project.c utilizando o seguinte comando:
  gcc so_project.c -o so_project
* Ao utilizar o código acima, será gerado 2 novos arquivos: so_project e teste.txt;
* O arquivo so_project é o binário do programa;
* O arquivo teste.txt é o resultado gerados das threads;

### Resultados ###

* A solução apresentada no programa utiliza uma variável de lock para regular o acesso dos processos às suas regiões críticas.
* O arquivo so_project_b.c apresenta o problema sem o tratamento de uma condição de corrida, fazendo com que apenas uma das threads consigam escrever no arquivo (teste.txt).
* O arquivo so_project.c apresenta o problema com a implementação de uma variável de lock, permitindo que ambas as threads gravem no arquivo (teste.txt) pois é regulado o acesso das mesmas às suas regiões críticas.


### Com quem falar em caso de dúvidas? ###

* brenohff@gmail.com
* guilhermemorum@gmail.com