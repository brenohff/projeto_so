
#define _GNU_SOURCE
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <sched.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>

#define FIBER_STACK 1024*64

int t1(void *argument);
int t2(void *argument);
bool lock = false;

int main (){
    pid_t thread1, thread2;
    void *stack;
    stack = malloc(FIBER_STACK);
    void *stack2;
    stack2 = malloc(FIBER_STACK);
		printf("criando threads \n");
	    thread1 = clone(&t1, (char*) stack + FIBER_STACK, SIGCHLD|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_VM, 0);
	    thread2 = clone(&t2, (char*) stack2 + FIBER_STACK, SIGCHLD|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_VM, 0);
	free(stack);
	free(stack2);


    return 0;

}

int t1(void *argument){
    while(lock){
	printf("Esperando 2 \n");
    }
    lock = true;
    printf("travando o lock em t1 \n");
    FILE *fp;
    fp = fopen("teste.txt", "a"); // Arquivo ASCII, para escrita 
    if(!fp){
        printf( "Erro na abertura do arquivo");
        exit(0);
    }else{
        fprintf(fp, "%s\n", "Thread1");
    }
    fclose(fp);
    lock = false;
    printf("liberando o lock em t1 \n");
    return 0;

}

int t2(void *argument){
    while(lock){
	printf("Esperando 1 \n");
    }
    lock = true;
    printf("travando o lock em t2 \n");
    FILE *fp;
    fp = fopen("teste.txt", "a"); // Arquivo ASCII, para escrita 
    if(!fp){
        printf( "Erro na abertura do arquivo");
        exit(0);
    }else{
        fprintf(fp, "%s\n", "Thread2");
    }

    fclose(fp);
    lock = false;
    printf("liberando o lock em t2 \n");
    return 0;

}
